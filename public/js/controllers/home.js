app
.controller('HomeCtrl', ['$scope','$http',
    function($scope, $http){
        $scope.setData = function(){
            if($scope.player_game == undefined || $scope.player_game.trim().length == 0){
                alert("player_game is empty");
            }
            else if($scope.date == undefined){
                alert("date is empty");
            }
            else if($scope.score == undefined){
                alert("score is empty");
            }
            else {
                $http({
                    url: baseUrl + '/index/set',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param({
                        player_game : $scope.player_game,
                        date        : $scope.date,
                        score       : $scope.score
                    })
                })
                .then(
                    function successCallback(response){
                        var data    =   response.data;
                        if (data.statusCode == 200) {
                            alert("User Created");
                            $scope.player_game	    =   '';
                            $scope.date		        =   '';
                            $scope.score		    =   '';
                        }
                    },
                    function errorCallback(responce){

                    }
                );
            }
        }
        $scope.getData = function(){
            $http({
                    url: baseUrl + '/index/get',
                    method: "GET",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    params: {}
                })
                .then(
                    function successCallback(response){
                        var data    =   response.data;
                        if (data.statusCode == 200) {
                            $scope.scores    =   data.devMessage;
                        }
                        else{

                        }
                    },
                    function errorCallback(responce){

                    }
                );
            }
        $scope.getData();
    }
])
