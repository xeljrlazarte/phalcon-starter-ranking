app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function ($stateProvider, $urlRouterProvider, $httpProvider) {
		var landing 	= 	'/';
		var tpl 		= 	'/index/home';
		var newState 	= 	'home';
		var newTitle 	= 	'dashboard';
		$urlRouterProvider.otherwise(landing);
		$stateProvider
			.state(newState,{
				url: 	landing,
				templateUrl: function(){ return baseUrl + tpl;},
				title: newTitle,
				resolve: {
					loadPlugin: function ($ocLazyLoad, $rootScope, $state) {
						return $ocLazyLoad.load ([
							{
								name: 'vendors',
								files: [
									baseUrl + '/js/controllers/home.js'
								]
							}
						])
					}
				}
			})

			.state('home.add',{
				url: 	'add',
				templateUrl: '/index/add',
				title: 'add',
				controller: 'HomeCtrl'
			})

			.state('home.view',{
				url: 	'view',
				templateUrl: '/index/view',
				title: 'add',
				controller: 'HomeCtrl'
			})

	}

]);
