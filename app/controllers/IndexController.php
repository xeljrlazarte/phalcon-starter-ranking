<?php
use Phalcon\Mvc\View,
    Phalcon\Mvc\Controller,
    Phalcon\Filter;
class IndexController extends ControllerBase{
    public function beforeExecuteRoute($dispatcher) {

    }
    public function indexAction(){
    }

    public function homeAction($asdasd){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function addAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function viewAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function setAction(){
        $this->view->disable();
        if ($this->request->isPost()) {

            $new                =   new Scores();
            $new->player_game   = $this->request->getPost('player_game');
            $new->date          = $this->request->getPost('date');
            $new->score         = $this->request->getPost('score');

            if($new->create()){
                $this->respond(array(
                    'statusCode'    => 200,
                    'devMessage'    => array(
                        'lastId'    => $new->id
                    )
                ));
            }
            else{
                foreach ($new->getMessages() as $error) {
                $devMessage[]   =      $error->getMessages();
                }
                $this->respond(array(
                    'statusCode'    => 500,
                    'devMessage'    => $devMessage
                ));
            }
        }
    }

    public function getAction(){
        $this->view->disable();
        if($this->request->isGet()){
            $scores = Scores::query()->orderBy('score DESC')->execute();
            if($scores){
                if($scores->count() != 0){
                    $data   =   [];
                    foreach ($scores as $row) {
                        $data [] = array(
                            'id'            =>  $row->id,
                            'player_game'   =>  $row->player_game,
                            'score'         =>  $row->score,
                            'date'          =>  $row->date
                            );
                    }
                    $this->respond(array(
                    'statusCode'        => 200,
                    'devMessage'        => $data
                    ));
                }
                else{
                    $this->respond(array(
                    'statusCode'        => 200,
                    'devMessage'        => ""
                    ));
                }
            }
            else{
                foreach ($users->getMessages() as $error) {
                    $devMessage[]   =      $error->getMessages();
                }
                $this->respond(array(
                'statusCode'        => 500,
                'devMessage'        => $devMessage
                ));
            }
        }
        else{

        }
    }

}
